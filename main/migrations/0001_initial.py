# Generated by Django 2.2.1 on 2021-08-13 23:30

from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('nickname', models.CharField(max_length=20)),
                ('profile_image', models.ImageField(blank=True, upload_to='images/')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Quiz',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quiz_id', models.IntegerField()),
                ('quiz_content', models.CharField(max_length=200, null=True)),
                ('quiz_img', models.ImageField(blank=True, null=True, upload_to='images/')),
                ('quiz_true', models.CharField(max_length=100)),
                ('quiz_false', models.CharField(max_length=100)),
                ('quiz_explanation', models.CharField(max_length=300, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='a_user', serialize=False, to=settings.AUTH_USER_MODEL)),
                ('activity_title', models.CharField(default='제목을 입력해주세요', max_length=45)),
                ('activity_img', models.ImageField(blank=True, null=True, upload_to='images/')),
                ('activity_content', models.TextField(max_length=400, null=True)),
                ('activity_date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='shop_user', serialize=False, to=settings.AUTH_USER_MODEL)),
                ('addchallenge_id', models.IntegerField(null=True)),
                ('theme_id', models.IntegerField(null=True)),
                ('theme_name', models.CharField(max_length=45, null=True)),
                ('theme_price', models.IntegerField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Challenge',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('challenge_name', models.CharField(max_length=45)),
                ('introduction', models.CharField(max_length=100, null=True)),
                ('challenge_start', models.DateTimeField(auto_now=True)),
                ('challenge_img', models.ImageField(blank=True, upload_to='images/')),
                ('categories', models.ManyToManyField(blank=True, to='main.Category')),
            ],
        ),
        migrations.CreateModel(
            name='Stamp',
            fields=[
                ('id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='stp_user', serialize=False, to=settings.AUTH_USER_MODEL)),
                ('activity_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='activity_id', to=settings.AUTH_USER_MODEL)),
                ('challenge_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='myapp.Stamp.challenge_id+', to='main.Challenge')),
            ],
        ),
        migrations.CreateModel(
            name='Quiz_mypage',
            fields=[
                ('id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='q_user', serialize=False, to=settings.AUTH_USER_MODEL)),
                ('quiz_nonepass', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='quiz_nonepass', to='main.Quiz')),
                ('quiz_pass', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='quiz_pass', to='main.Quiz')),
            ],
        ),
        migrations.CreateModel(
            name='Point',
            fields=[
                ('id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='p_user', serialize=False, to=settings.AUTH_USER_MODEL)),
                ('user_point', models.IntegerField()),
                ('point_log_name', models.ForeignKey(max_length=45, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='point_log_name', to='main.Shop')),
                ('point_log_price', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='point_log_price', to='main.Shop')),
            ],
        ),
        migrations.CreateModel(
            name='N_badge',
            fields=[
                ('id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='nb_user', serialize=False, to=settings.AUTH_USER_MODEL)),
                ('n30_badge', models.BooleanField(default=True, null=True)),
                ('n50_badge', models.BooleanField(default=True, null=True)),
                ('n70_badge', models.BooleanField(default=True, null=True)),
                ('n100_badge', models.BooleanField(default=True, null=True)),
                ('badge_date', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='badge_date', to='main.Activity')),
                ('challenge', models.ForeignKey(max_length=20, on_delete=django.db.models.deletion.CASCADE, related_name='challenge', to='main.Challenge')),
            ],
        ),
        migrations.CreateModel(
            name='Challenge_mypage',
            fields=[
                ('id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='c_user', serialize=False, to=settings.AUTH_USER_MODEL)),
                ('challenge_ing', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='challenge_ing', to='main.Challenge')),
                ('challenge_past', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cmychallenge_past', to='main.Challenge')),
            ],
        ),
        migrations.CreateModel(
            name='Challenge_Badge',
            fields=[
                ('id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='cb_user', serialize=False, to=settings.AUTH_USER_MODEL)),
                ('challenge_past', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cbchallenge_past', to='main.Challenge_mypage')),
            ],
        ),
        migrations.AddField(
            model_name='activity',
            name='challenge_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='myapp.Activity.challenge_id+', to='main.Challenge'),
        ),
    ]
